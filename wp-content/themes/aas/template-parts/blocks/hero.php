<?php
	$hero = get_field('hero_block'); 
	$image_pre = $hero['bg_image'];
	$bg = $image_pre['sizes']['large'];
	$title = $hero['title'];
	// button 1
	$b_1 = $hero['button_1'];
	$b_1_title = $b_1['title'];
	$b_1_url = $b_1['url'];
	// button 2
	$b_2 = $hero['button_2'];
	$b_2_title = $b_2['title'];
	$b_2_url = $b_2['url'];
?>
<div class="hero-main-cont">
	<img name="Hero Background" src="<?php echo $bg; ?>">
	<div class="hero-inner-cont">
		<div class="hero-info-cont">
			<h1 class="white"><?php echo $title; ?></h1>
			<div class="hero-button-cont">
				<a class="right-angle white button-hover" href="<?php echo $b_1_url; ?>"><?php echo $b_1_title; ?></a>
				<a class="left-angle white button-hover" href="<?php echo $b_2_url; ?>"><?php echo $b_2_title; ?></a>
			</div>
		</div>
	</div>
</div>