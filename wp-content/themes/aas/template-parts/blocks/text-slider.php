<?php
	
?>
<div class="text-slider-main-cont">
	<?php

		$data = get_field('text-slider');

		if( have_rows('text-slider') ) :
			?>
			<div class="slider-for">
				<?php
					while ( have_rows('text-slider') ) : the_row();
						//do work here
						$para = get_sub_field('paragraph');
						$person = get_sub_field('person');
						$company = get_sub_field('company');
					?>

					<div class="text-cont">
						<h4 class="blue5"><?php echo $para; ?></h4>
						<p class="right-angle-grey"><?php echo $person  . ', ' . $company; ?></p>
					</div>

					<?php 
					endwhile;

					?>
			</div>
			<div class="slider-nav">
				<?php
					//nav
					while(have_rows('text-slider') ) : the_row();
				?>
					<p></p>
				<?php 
					endwhile;
				?>
			</div>
			<?php

		endif;
	?>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
		HandleGalleryNav();
	});

	function HandleGalleryNav(){
		//defining the browswer width
		var width = window.innerWidth;

		//large size
		if(width > 1050){
			$('.slider-for').slick({
				autoplay: true,
				autoplaySpeed: 5000,
				slidesToShow: 1,
				slidesToScroll: 1,
				fade: true,
				asNavFor: '.slider-nav',
  				pauseOnHover:false
			});
			$('.slider-nav').slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				arrows: false,
				dots: false,
				centerMode: true,
				focusOnSelect: true,
				pauseOnHover:false
			});
		}
		else{
			$('.slider-for').slick({
				autoplay: true,
				autoplaySpeed: 5000,
				slidesToScroll: 1,
				fade: true,
				asNavFor: '.slider-nav',
				pauseOnHover:false
			});
			$('.slider-nav').slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				arrows: false,
				dots: false,
				centerMode: true,
				focusOnSelect: true,
				pauseOnHover:false
			});
		}
	}
</script>