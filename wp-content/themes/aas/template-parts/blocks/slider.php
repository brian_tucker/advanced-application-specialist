<?php

	// $single = false;
	$single = get_field('single_slide'); 

	if($single == true){
		$args = array(
		    'post_type'      => 'projects',
		    'posts_per_page' => 1,
		    'paged'=> $paged,
		    'post_parent'    => $post->ID,
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
		 );
		$projects = new WP_Query( $args );
	}
	else{
		$args = array(
		    'post_type'      => 'projects',
		    'posts_per_page' => -1,
		    'paged'=> $paged,
		    'post_parent'    => $post->ID,
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order'
		 );
		$projects = new WP_Query( $args );
	}
?>


<?php
if($single == true){

?>
	<div class="slider-main-cont featured-main-cont">
		<div class="slider-inner-cont featured-inner-cont">
			<?php

				//if the post has any children, then it will
				if( $projects -> have_posts() ) :
				 while ( $projects->have_posts() ) : $projects->the_post(); ?>
	        		<?php

	        			//getting the id and setting the project data field
	        			$project_id = get_the_ID();
	        			// $project_data = get_field('project_data', $project_id);
	        			$title = get_the_title();
	        			$excerpt = get_the_excerpt();
	        			$image = get_the_post_thumbnail_url();

	        			//setting the project data field
	        			$project_data = get_field('project_data', $project_id);
	        			$slider_ex = $project_data['slider_excerpt'];
	        			$before = $project_data['before']['sizes']['medium'];
	        			// $before = $project_data['sizes']['medium'];
	        			$after = $project_data['after']['sizes']['medium'];
        			?>
	        			<div class="slider-card featured-card">
		        			<div class="slider-left featured-left">
								<div class="slider-left-inner featured-left-inner">
									<img class="slider-after-img" src="<?php echo $after; ?>">
									<div class="slider-sec-img">
										<!-- need to put the before image here -->
										<img class="slider-before-img" src="<?php echo $before; ?>">
									</div>
								</div>
							</div>
							<div class="slider-right featured-right">
								<div class="slider-right-inner featured-right-inner">
									<p class="category right-angle-grey">Category, Category</p>
									<h3 class="blue5"><? the_title(); ?></h3>

									<?if($slider_ex != ""){
									?>
										<p><?php echo $slider_ex; ?></p>
									<?php
									}
									else{
									?>
										<?php the_excerpt(); ?>
									<?php
									}?>
									
									<a class="orange-bg button-hover" href="<?php echo get_the_permalink(); ?>">View Project</a>
								</div>
							</div>
						</div>
				<?php endwhile; ?>			
		   	<?php endif; wp_reset_postdata(); ?>
		</div>
	</div>
<?php } else{ ?>
	<div class="slider-main-cont featured-main-cont">
		<div class="slider-inner-cont featured-inner-cont slider-for">
			<?php

				//if the post has any children, then it will
				if( $projects -> have_posts() ) :
				 while ( $projects->have_posts() ) : $projects->the_post(); ?>
	        		<?php

	        			//getting the id and setting the project data field
	        			$project_id = get_the_ID();
	        			// $project_data = get_field('project_data', $project_id);
	        			$title = get_the_title();
	        			$excerpt = get_the_excerpt();
	        			$image = get_the_post_thumbnail_url();

	        			//setting the project data field
	        			$project_data = get_field('project_data', $project_id);
	        			$slider_ex = $project_data['slider_excerpt'];
	        			$before = $project_data['before']['sizes']['medium'];
	        			// $before = $project_data['sizes']['medium'];
	        			$after = $project_data['after']['sizes']['medium'];

	        			?>
	        			<div class="slider-card featured-card ">
		        			<div class="slider-left featured-left">
								<div class="slider-left-inner featured-left-inner">
									<img class="slider-after-img" src="<?php echo $after; ?>">
									<div class="slider-sec-img">
										<!-- need to put the before image here -->
										<img class="slider-before-img" src="<?php echo $before; ?>">
									</div>
								</div>
							</div>
							<div class="slider-right featured-right">
								<div class="slider-right-inner featured-right-inner">
									<p class="category right-angle-grey">Category, Category</p>
									<h3 class="blue5"><? the_title(); ?></h3>

									<?if($slider_ex != ""){
									?>
										<p><?php echo $slider_ex; ?></p>
									<?php
									}
									else{
									?>
										<?php the_excerpt(); ?>
									<?php
									}?>

									<div class="inner-bottom-cont">
										<a class="orange-bg" href="<?php echo get_the_permalink(); ?>">View Project</a>	
										<!-- <div class="slider-nav"></div> -->
									</div>
								</div>
							</div>
						</div>
				<?php endwhile; ?>			
		   	<?php endif; wp_reset_postdata(); ?>
		</div>

		<div class="slider-nav">
		<?php	if( $projects -> have_posts() ) :
					 while ( $projects->have_posts() ) : $projects->the_post(); ?>
		        		<p></p>
	        		<?php endwhile; ?>			
		   	<?php endif; wp_reset_postdata(); ?>
			</div>
		</div>
<?php }?>

<script type="text/javascript">
	$(document).ready(function(){
		HandleGalleryNav();
	});

	function HandleGalleryNav(){
		//defining the browswer width
		var width = window.innerWidth;

		//large size
		if(width > 1050){
			$('.slider-for').slick({
				autoplay: true,
				autoplaySpeed: 5000,
				slidesToShow: 1,
				slidesToScroll: 1,
				fade: true,
				asNavFor: '.slider-nav',
				pauseOnHover:false
			});
			$('.slider-nav').slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				arrows: false,
				dots: false,
				centerMode: true,
				focusOnSelect: true,
				pauseOnHover:false
			});
		}
		else{
			$('.slider-for').slick({
				autoplay: true,
				autoplaySpeed: 5000,
				slidesToScroll: 1,
				fade: true,
				asNavFor: '.slider-nav',
				pauseOnHover:false
			});
			$('.slider-nav').slick({
				slidesToShow: 2,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				arrows: false,
				dots: false,
				centerMode: true,
				focusOnSelect: true,
				pauseOnHover:false
			});
		}
	}
</script>