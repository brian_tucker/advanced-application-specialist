<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php
	$image = get_the_post_thumbnail_url();
	$title = get_the_title();
	$excerpt = get_the_excerpt();

	//setting the project data field
	// $project_data = get_field('project_data', $project_id);
?>

<div class="featured-card" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog-first-left-cont featured-left">
		<div class="featured-left-inner">
			<?php the_post_thumbnail(get_the_ID(), 'medium');?>
		</div>
	</div>

	<div class="blog-first-right-cont featured-right">
		<div class="featured-right-inner">
			<p class="category right-angle-grey">Featured Blog</p>
			<h3 class="blue5"><? the_title(); ?></h3>
			<?php the_excerpt(); ?>
			
			<a class="orange-bg button-hover" href="<?php echo get_the_permalink(); ?>">View Project</a>
		</div>
	</div>
</div>
