<?php
	$id = get_the_ID();
?>

<div class="archive-card sp-main-cont cell small-12 medium-6 large-4">
	<div class="bg-overlay-blue"></div>
	<div class="bg-cont">
		<div class="bg-inner-cont">
			<?php the_post_thumbnail($id, 'medium'); ?>
		</div>
	</div>
	<div class="inner-cont">
		<h3 class="white bottom-margin-cushion"><?php the_title(); ?></h3>
		<a class="orange-btn button-hover" href="<?php echo get_the_permalink(); ?>">explore</a>
	</div>	
</div>