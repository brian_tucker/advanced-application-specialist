<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="archive-card single-blog-main cell small-12 medium-6 large-4">
	<div class="bg-overlay-blue"></div>
	<div class="bg-cont">
		<?php the_post_thumbnail($id, 'medium'); ?>
	</div>
	<div class="inner-cont">
		<h3 class="white bottom-margin-cushion"><?php the_title(); ?></h3>
		<a class="orange-btn button-hover" href="<?php echo get_the_permalink(); ?>">explore</a>
	</div>	
</div>
