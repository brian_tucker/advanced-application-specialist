<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

	$general_data = get_field('general_data', 'options');

	$logo = $general_data['footer_logo']['sizes']['medium'];
	$logo_0 = $general_data['footer_logo_2']['sizes']['medium'];
	$social = $general_data['social'];
?>

<footer class="footer-container">
	<div class="footer-grid">
		<div class="footer-upper">

			<div class="upper-left">
				<!-- logo here -->
				<img src="<?php echo $logo; ?>">
				<div class="mobile-copy-cont">
					<p>Copyright &copy; 2020 Advanced Applications Specialists</p>
					<a href="">Terms</a>
					<a href="">Privacy Policy</a>
					<a href="https://gatorworks.net/">Site by Gatorworks</a>
				</div>
			</div>

			<div class="upper-right">
				<div class="r-left">
					<!-- <a href="">One</a>
					<a href="">Two</a>
					<a href="">Three</a>
					<a href="">Four</a>
					<a href="">Five</a>
					<a href="">Six</a> -->
					<?php foundationpress_footer_nav(); ?>
				</div>
				<div class="r-right">
					<img src="<?php echo $logo_0; ?>">
				</div>
			</div>
		</div>

		<div class="footer-lower">
			<div class="lower-left">
				<div class="social-cont">
					<?php
						if ( !empty( $social ) ) :
						    $count = count($social);

						    for($x = 0; $x < $count; $x++){
						    	?>
						    	<a href="<?php 

						    		if($social[$x]['social_link'] != ''){
						    			echo $social[$x]['social_link']['url'];
						    		}
						    		
						    		?>" class="social-icon white" href=""><?php echo $social[$x]['social_icon']; ?></a>
						    	<?php
						    }

						endif;
					?>
				</div>
			</div>
			<div class="lower-right">
				<div class="l-left">
					<p>Copyright &copy; 2020 Advanced Applications Specialists</p>
					<div class="vl"></div>
					<a href="">Terms</a>
					<div class="vl"></div>
					<a href="">Privacy Policy</a>
					<div class="vl"></div>
					<a href="https://gatorworks.net/">Site by Gatorworks</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php // if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php // endif; ?>

<?php wp_footer(); ?>

<?php if (strpos(get_bloginfo('url'), '.local')) { ?>
<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>".replace("HOST", location.hostname));
//]]></script>
<?php } ?> 

<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>

</body>
</html>
