<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php 
	//getting the page id using the name, the archive doesn't have an id
	$page_id = get_page_by_title('projects')->ID;
	//getting the header para
	$page_data = get_field('project_archive_data', $page_id); 
	$masonry = $page_data['masonry'];
	$excerpt = $page_data['excerpt'];
?>

<div class="default-cover-cont">
	<div class="bg-overlay-blue"></div>
	<?php echo get_the_post_thumbnail($page_id, 'large', 'style=max-width:100%;height:auto;'); ?>
	<h1 class="white  entry-title"><?php echo get_the_title($page_id); ?></h1>
	<?php if($excerpt != ""){?>
		<p class="white"><?php echo $excerpt; ?></p>
	<?php  } ?>
</div>

<div class="archive-projects-main-cont">
<?php 

	//getting the page id using the name, the archive doesn't have an id
	$page_id = get_page_by_title('projects')->ID;

	if($masonry != true){
?>	
	<div id="parent-<?php the_ID(); ?>" class="archive-projects-main-innercont grid-container">
		<div class="archive-card-cont grid-x grid-margin-x">
			<?php
				// WP_Query arguments
				$args = array(
					'post_type'              => array( 'projects' ),
					'order'                  => 'ASC',
					'orderby'                => 'menu_order',
					'post_parent'			 => 0
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						get_template_part( 'template-parts/content-sp');
					}
				} else {
					// no posts found
				}

				// Restore original Post Data
				wp_reset_postdata();
			?>
		</div>
	</div>	

	<?php } else{

		// echo 'yes';
		?>
		<div id="parent-<?php the_ID(); ?>" class="archive-projects-main-innercont grid-container">
			<div class="archive-card-cont grid">
				<?php
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'projects' ),
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						'post_parent'			 => 0
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							get_template_part( 'template-parts/content-masonry');
						}
					} else {
						// no posts found
					}

					// Restore original Post Data
					wp_reset_postdata();
				?>
			</div>
		</div>	
		<?php
	}?>
</div>

<?php get_footer();