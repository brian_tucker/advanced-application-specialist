<?php
/*
Template Name: Contact
*/
get_header(); ?>

<?php
	$general_data = get_field('general_data', 'options');
	$social = $general_data['social'];
	$address_line_1 = $general_data['address_line_1'];
	$address_line_2 = $general_data['address_line_2'];
	$office_phone = $general_data['office_phone'];
	$fax_phone = $general_data['fax_phone'];
	$email = $general_data['email'];

?>

<div class="default-cover-cont">
	<div class="bg-overlay-blue"></div>
	<?php the_post_thumbnail($id, 'large'); ?>
	<h1 class="white entry-title"><?php the_title(); ?></h1>
</div>
<div class="contact-main-cont main-container">
	<div class="main-grid">
		<main class="main-content-full-width">
			<div class="contact-info-cont">
				<div class="contact-left">
					<h5 class="right-angle-grey">Address</h5>
					<p class="grey4"><?php echo $address_line_1; ?></p>
					<p class="grey4"><?php echo $address_line_2; ?></p>
					<h5 class="right-angle-grey">Phone</h5>
					<p class="grey4"><?php echo $office_phone; ?></p>
					<p class="grey4"><?php echo $fax_phone; ?></p>
					<h5 class="right-angle-grey">Email</h5>
					<p class="grey4"><?php echo $email; ?></p>
					<h5 class="right-angle-grey">Social</h5>
					<div class="social-contact-cont">
						<?php
						if ( !empty( $social ) ) :
						    $count = count($social);

						    for($x = 0; $x < $count; $x++){
						    	?>
						    	<a href="<?php 

						    		if($social[$x]['social_link'] != ''){
						    			echo $social[$x]['social_link']['url'];
						    		}
						    		
						    		?>" class="social-icon blue2" href=""><?php echo $social[$x]['social_icon']; ?></a>
						    	<?php
						    }

						endif;
					?>
					</div>
				</div>
				<div class="contact-right">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13762.584376382958!2d-91.0096539!3d30.4177814!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8e6002cdfae6622c!2sBelzona%20Baton%20Rouge!5e0!3m2!1sen!2sus!4v1601042867153!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</main>
	</div> 
</div>
<?php get_footer();
