<?php
/*
Template Name: Default
*/
get_header(); ?>

<?php
	$id = get_the_ID();
?>
<div class="default-cover-cont">
	<div class="bg-overlay-blue"></div>
	<?php the_post_thumbnail($id, 'large'); ?>
	<h1 class="white entry-title"><?php the_title(); ?></h1>
</div>
<div class="default-main-cont main-container">
	<div class="main-grid">
		<main class="main-content-full-width">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</main>
	</div>
</div>
<?php get_footer();
