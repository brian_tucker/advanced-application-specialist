<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
	$id = get_page_by_title('blog')->ID;
?>
<div class="default-cover-cont">
	<div class="bg-overlay-blue"></div>
	<?php the_post_thumbnail($id, 'large'); ?>
	<h1 class="white"><?php echo get_the_title($id); ?></h1>
</div>

<div class="main-container blog-main-cont">
	<div class="main-grid">
		<main class="main-content">
			<div class="blog-first-cont featured-main-cont">
				<div class="featured-inner-cont">
					<?php

						$single_args = array(
							'post_type' => 'post',
							'posts_per_page' => 1
						);
						$single_query = new WP_query ( $single_args );

					?>
					<?php if ( $single_query->have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( $single_query->have_posts() ) : $single_query->the_post(); ?>
							<?php get_template_part( 'template-parts/content-single', get_post_format() ); ?>
						<?php endwhile; ?>

						<?php else : ?>
							<?php get_template_part( 'template-parts/content', 'none' ); ?>

					<?php endif; // End have_posts() check. ?>
				</div>
			</div>
			<div class="blog-rest-cont grid-x grid-margin-x"> 
				
				<?php

					$args = array(
						'post_type' => 'post',
						'post_parent' => 0,
						'paged' => get_query_var('paged') ? get_query_var('paged') : 1
						// 'offset' => 1
					);
					$query = new WP_query ( $args );
				?>

					<?php

						// The Query

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();

								get_template_part( 'template-parts/content');
								
							}
						} else {
							// no posts found
						}

						// Restore original Post Data
						wp_reset_postdata();
					?>
			</div>


			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php
			if ( function_exists( 'foundationpress_pagination' ) ) :
				foundationpress_pagination();
			elseif ( is_paged() ) :
			?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php endif; ?>

		</main>
		<!-- <?php get_sidebar(); ?> -->

	</div>
</div>
<?php get_footer();