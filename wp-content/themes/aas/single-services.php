<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
	
	//getting the page id using the name, the archive doesn't have an id
	$page_id = get_page_by_title('projects')->ID;
?>

<div class="default-cover-cont">
	<div class="bg-overlay-blue"></div>
	<?php the_post_thumbnail($id, 'large'); ?>
	<h1 class="white"><?php the_title(); ?></h1>
</div>


<div class="project-single-main-cont sp-single-main-cont">
	<div class="project-single-inner-cont">
		<!-- this is the stuff from the backend -->
		<main class="single-program-entry-cont">
			<!-- This section displays the posts for this custom post type but I'm just gonna use gutenberg -->
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<?php endwhile; ?>
		</main>
	</div>
</div>

<?php get_footer();