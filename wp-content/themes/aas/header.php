<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
$general_data = get_field('general_data', 'options');

$logo = $general_data['logo']['sizes']['medium'];

$phone = $general_data['phone_number'];
$social = $general_data['social'];

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	
		<!-- Add the slick-theme.css if you want default styling -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
		<!-- Add the slick-theme.css if you want default styling -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
	</head>
	<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<header class="site-header" role="banner">
		<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
			<div class="title-bar-left">
				
				<span class="site-mobile-title title-bar-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $logo; ?>"></a>
				</span>
			</div>
		</div>

		<nav class="site-navigation top-bar" role="navigation" id="<?php foundationpress_mobile_menu_id(); ?>">
			<div class="top-bar-left">
				<div class="top-bar-left-menu">
					<?php foundationpress_top_bar_l(); ?>
				</div>
			</div>
			<div class="top-bar-center">
				<div class="site-desktop-title top-bar-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img id="main-logo" src="<?php echo $logo; ?>"></a>
				</div>
			</div>
			<div class="top-bar-right">
				<div class="top-bar-right-menu">
					<?php foundationpress_top_bar_r(); ?>
				</div>
				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>

		<div class="hide-for-large" id="mobile-menu-jhl">
			<div class="mobile-menu-top">
				<div class="logo-container">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img id="main-logo" src="<?php echo $logo; ?>"></a>
				</div>
			</div>
			<div class="menu-inner-toggle-cont menu-toggle-cont menu-icon">
				<div class="menu-toggle-inner">
					<div class="menu-toggle">
						<a class="white" href="">Menu</a>
					</div>
				</div>
			</div>
			
			<?php foundationpress_mobile_nav() ?>

			<div class="mobile-phone-cont">
				<a class="mobile-phone" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
			</div>

			<div class="mobile-menu-social-cont">
				<?php
					if ( !empty( $social ) ) :
					    $count = count($social);

					    for($x = 0; $x < $count; $x++){
					    	?>
					    	<a href="<?php 

					    		if($social[$x]['social_link'] != ''){
					    			echo $social[$x]['social_link']['url'];
					    		}
					    		
					    		?>" class="social-icon white" href=""><?php echo $social[$x]['social_icon']; ?></a>
					    	<?php
					    }

					endif;
				?>
			</div>
		</div>

		<div class="menu-toggle-cont menu-icon">
			<div class="menu-toggle-inner">
				<div class="menu-toggle">
					<span class="white">Menu</span>
				</div>
			</div>
		</div>

		<script>
			$(".menu-icon").on('click', function(){

				$("#mobile-menu-jhl").toggleClass("show-mobile-nav");
			});

			$("#mobile-menu-jhl .menu .menu-item-has-children ul").addClass('is-active');
		</script>
	</header>